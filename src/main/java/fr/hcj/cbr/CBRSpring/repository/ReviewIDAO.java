package fr.hcj.cbr.CBRSpring.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Review;

public interface ReviewIDAO extends CrudRepository<Review, Long>{
	Set<Review> findByClient(Client client);
	Review save(Review review);
	
	
	@Query(value = "SELECT r.* "
			+ "FROM review r left join reservation re on re.idreservation=r.idreservation "
			+ "left join bedroom b on b.idbedroom=re.idbedroom "
			+ "left join hotel h on h.idhotel = b.idhotel "
			+ "Where h.idhotel = :hotelParam", nativeQuery = true)
	Set<Review> getReviewByIdHotel(@Param("hotelParam")Integer idhotel); 
}
