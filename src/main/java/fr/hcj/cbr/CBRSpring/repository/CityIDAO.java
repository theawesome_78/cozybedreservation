package fr.hcj.cbr.CBRSpring.repository;

import org.springframework.data.repository.CrudRepository;


import fr.hcj.cbr.CBRSpring.model.City;

public interface CityIDAO extends CrudRepository<City, Long>{
	City findByCityname (String cityname);
	City findByIdcity (Integer idcity);
}
