package fr.hcj.cbr.CBRSpring.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.Hotel;

public interface BedroomIDAO extends CrudRepository<Bedroom	, Long>{
	
	Set<Bedroom> findByHotel(Hotel hotel);
	Bedroom findByIdbedroom(Integer idbedroom);
	
	@Query(value = "SELECT b.* "
			+ "FROM bedroom b LEFT JOIN hotel h on h.idhotel = b.idhotel "
			+ "LEFT JOIN city c ON c.idcity = h.idcity "
			+ "LEFT JOIN reservation r on r.idbedroom = b.idbedroom "
			+ "WHERE c.cityname =:cityParam "
			+ "AND b.capacity >=:numguestParam "
			+ "AND h.idhotel = :idhotelParem "
			+ "AND b.idbedroom NOT IN ("
			+ "SELECT b.idbedroom FROM bedroom b LEFT JOIN reservation r on (r.idbedroom = b.idbedroom and r.available = 1 ) "
			+ "WHERE  (DATEDIFF(:checkinParam, r.checkoutdate) <0 and DATEDIFF(:checkoutParam, r.checkindate )>0 )"
			+ "OR  (DATEDIFF(:checkinParam,r.checkoutdate ) <0 and DATEDIFF( :checkoutParam,r.checkindate)>0) "
			+ "OR (DATEDIFF(:checkinParam, r.checkindate ) <=0  AND  DATEDIFF(:checkoutParam,r.checkoutdate) >=0))",nativeQuery = true ) 
	Set<Bedroom> findAvailableBedroomByHotel(
			@Param("cityParam")String city, @Param("numguestParam")Integer numberguest, 
			@Param("checkinParam")String checkin, @Param("checkoutParam")String checkout,
			@Param("idhotelParem") Integer idhotel );
	
	@Query(value ="SELECT * FROM bedroom"
			+ "where idhotel = :idhotelParam order by price", nativeQuery = true)
	Set<Bedroom> findyMinimumPriceInHotelBedrooms(@Param("idhotelParam")Integer idhotel);
}
