package fr.hcj.cbr.CBRSpring.repository;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Reservation;
import fr.hcj.cbr.CBRSpring.model.Review;

public interface ClientIDAO extends CrudRepository<Client, Long> {
	
	@Query("SELECT c FROM Client c WHERE c.login = :loginParam")
	Client getClientByLogin(@Param("loginParam") String login);

	Client findByIdclient(Integer idclient);
	
	Client save(Client client);
	
	Client findByLoginIs(String login);
}
