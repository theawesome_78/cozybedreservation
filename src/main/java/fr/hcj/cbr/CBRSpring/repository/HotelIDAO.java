package fr.hcj.cbr.CBRSpring.repository;


import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.City;
import fr.hcj.cbr.CBRSpring.model.Hotel;

public interface HotelIDAO extends CrudRepository<Hotel, Long>{
	
	
	@Query(value = "SELECT h.* "
			+ "FROM bedroom b LEFT JOIN hotel h on h.idhotel = b.idhotel "
			+ "LEFT JOIN city c ON c.idcity = h.idcity "
			+ "LEFT JOIN reservation r on r.idbedroom = b.idbedroom "
			+ "WHERE c.cityname =:cityParam "
			+ "AND b.capacity >=:numguestParam "
			+ "AND b.idbedroom NOT IN ("
			+ "SELECT b.idbedroom FROM bedroom b LEFT JOIN reservation r on (r.idbedroom = b.idbedroom and r.available = 1 ) "
			+ "WHERE  (DATEDIFF(:checkinParam, r.checkoutdate) <0 and DATEDIFF(:checkoutParam, r.checkindate )>0 )"
			+ "OR  (DATEDIFF(:checkinParam,r.checkoutdate ) <0 and DATEDIFF( :checkoutParam,r.checkindate)>0) "
			+ "OR (DATEDIFF(:checkinParam, r.checkindate ) <=0  AND  DATEDIFF(:checkoutParam,r.checkoutdate) >=0))",nativeQuery = true ) 
	Set<Hotel> getHotelsByCityCapacityCheckinCheckout(
			@Param("cityParam")String city, @Param("numguestParam")Integer numberguest, 
			@Param("checkinParam")String checkin, @Param("checkoutParam")String checkout
			);
	
	
	
	@Query(value = "SELECT h FROM Hotel h")
	Set<Hotel> getAllHotels();
	
	Set<Hotel> findByCity(City city);
	
	Hotel findByIdhotel(Integer idhotel);
	
	@Query(value ="SELECT h.* from hotel h LEFT JOIN city c on c.idcity = h.idcity left join bedroom b on b.idhotel =h.idhotel where b.capacity >=:numguestParam and c.cityname =:cityParam",nativeQuery = true )
	Set<Hotel> getHotelsByCitynameAndCapacity(@Param("cityParam")String city, @Param("numguestParam")Integer numberguest);
	
	
	//@Query(value = "SELECT h.* FROM bedroom b LEFT JOIN reservation r on r.idbedroom = b.idbedroom LEFT JOIN hotel h on h.idhotel = b.idhotel WHERE NOT(r.checkoutdate <= '2022-01-09 00:00:00' OR '2022-02-21 00:00:00'<= r.checkindate) AND r.available = 1" , nativeQuery = true)
	//@Query(value = "SELECT h.* FROM bedroom b LEFT JOIN reservation r on (r.idbedroom = b.idbedroom AND r.available = 1) LEFT JOIN hotel h on h.idhotel = b.idhotel WHERE (  DATEDIFF(r.checkoutdate,'2022-01-10') <=0 OR DATEDIFF('2022-01-10',r.checkindate) <=0 OR idreservation is Null  ) group by h.idhotel", nativeQuery = true)
	@Query(value = "SELECT h.* FROM bedroom b "
			+ "LEFT JOIN hotel h on h.idhotel = b.idhotel "
			+ "LEFT JOIN city c ON c.idcity = h.idcity "
			+ "WHERE c.cityname =:cityParam "
			+ "AND b.capacity >=:numguestParam "
			+ "AND b.idbedroom NOT IN "
			+ "(SELECT b.idbedroom FROM bedroom b "
			+ "LEFT JOIN reservation r on (r.idbedroom = b.idbedroom and r.available = 1 ) "
			+ "WHERE  (DATEDIFF( :checkinParam, r.checkoutdate ) <=0  OR  DATEDIFF(r.checkindate, :checkoutParam) <=0) "
			+ "AND (DATEDIFF(:checkinParam, r.checkindate ) >=0  AND  DATEDIFF(r.checkoutdate, :checkoutParam) >=0)) "
			+ "group by h.idhotel", nativeQuery = true)
	Set<Hotel> test(@Param("cityParam")String city, @Param("numguestParam")Integer numberguest, 
			@Param("checkinParam")String checkin, @Param("checkoutParam")String checkout
			);
	
	@Query(value ="SELECT h.* FROM bedroom b LEFT JOIN hotel h on h.idhotel = b.idhotel LEFT JOIN city c ON c.idcity = h.idcity WHERE b.idbedroom NOT IN (SELECT b.idbedroom FROM bedroom b LEFT JOIN reservation r on (r.idbedroom = b.idbedroom and r.available = 1 ) WHERE  (DATEDIFF('2022-04-10', r.checkoutdate ) <=0  OR  DATEDIFF(r.checkindate, '2022-04-12') <=0) AND (DATEDIFF('2022-04-10', r.checkindate ) >=0  AND  DATEDIFF(r.checkoutdate, '2022-04-12') >=0)) group by h.idhotel", nativeQuery = true)
	Set<Hotel> test2();
	
}
