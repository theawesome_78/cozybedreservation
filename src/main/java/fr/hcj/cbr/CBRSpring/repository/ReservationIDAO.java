package fr.hcj.cbr.CBRSpring.repository;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Reservation;

public interface ReservationIDAO extends CrudRepository<Reservation, Long>{
	
	@Query(value = "INSERT INTO `reservation` VALUES (null,true,:checkinParam,:checkoutParam,:capacityParam,:idbedroomParam,:idclientParam,NULL);" ,
			nativeQuery = true)
	void createReservation(@Param("checkinParam")LocalDate checkindate, @Param("checkoutParam")LocalDate checkoutdate, 
			@Param("capacityParam")Integer capacity, @Param("idbedroomParam")Bedroom bedroom,
			@Param("idclientParam")Client client);
	
	
	Reservation save(Reservation reserv);
	
	Set<Reservation> findByClient(Client client);
	
	Reservation findByIdreservation(Integer idreservation);
}
