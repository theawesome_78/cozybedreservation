package fr.hcj.cbr.CBRSpring.controller;

import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Reservation;
import fr.hcj.cbr.CBRSpring.model.Review;
import fr.hcj.cbr.CBRSpring.service.AccountIBusiness;
import fr.hcj.cbr.CBRSpring.service.ClientIBusiness;
import fr.hcj.cbr.CBRSpring.service.ReservationIBusiness;
import fr.hcj.cbr.CBRSpring.service.ReviewIBusiness;
import net.bytebuddy.asm.Advice.Local;

@Controller(value = "mbClientAccount")
@Scope(value = "session")
public class ClientAccountManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;


	@Autowired
	private ReservationIBusiness reservationBusiness;
	@Autowired
	private ClientIBusiness clientBusiness;
	@Autowired
	private ReviewIBusiness reviewBusiness;
	@Autowired
	private AccountIBusiness accountBusiness;

	private Client client;
	private String login;
	private Client updatingclient;
	private String password;
	private String adressmail;
	private Integer phonenumber;
	private Set<Review> reviews;
	private Set<Reservation> reservations;
	private Integer rating;
	private String comment;
	private Reservation reservation;
	
	private String newlogin;	
	private String newpassword;
	private String confirmnewpassword;
	private String newsurname;
	private String newname;
	private String newbirthdaystring;
	private String newadressmail;
	private Integer newphonenumber;
	

	
	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		client = (Client) session.getAttribute("connectedClient");
	}
	
	public boolean inputSignupCheck(String newlogin) {
		boolean inputSignupCheck = false;
		if (clientBusiness.findClientByLogin(newlogin) == null) {
			System.out.println("ok pour login");
			inputSignupCheck = true;
		}else {
			System.out.println("login identique");
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN ,
					"Votre login existe déjà", "Votre login existe déjà");
			FacesContext.getCurrentInstance().addMessage("signupForm:inpsignup", facesMessage);
			
		}
		return inputSignupCheck;
	}
	
	public String connect() throws NoSuchAlgorithmException {
		String forward;
//		System.out.println("connect() engagé");
//		System.out.println(login);
//		System.out.println(password);
		client = accountBusiness.connect(login, password);
		
		if (client != null) {
			System.out.println("dans premier if");
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
			session.setAttribute("connectedClient", client);
			//redirectionAfterLog(isConnected());
			forward = "/clientmain.xhtml?faces-redirection=true";
		} else {
			System.out.println("dans else");
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN ,
					"Identifiant et/ou mot de passe incorrect(s)", "Identifiant et/ou mot de passe incorrect(s)");
			FacesContext.getCurrentInstance().addMessage("loginForm2:inpLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm2:inpPassword", facesMessage);
			//redirectionAfterLog(isConnected());
			forward = "/login.xhtml?faces-redirection=true";
		}
		login = null;
		password = null;
		return forward;
		
	}

	public String getReservationByClient() {


		reservations = reservationBusiness.findClientReservation(client);

		return "/clientreservation.xhtml?faces-redirection=true";

	}

	public String updateClient() throws NoSuchAlgorithmException {

		updatingclient = client;

		if (password != "") {
			String updatedHashedPassword = accountBusiness.hashPassword(client.getCumin(), password);
			updatingclient.setPassword(updatedHashedPassword);
			System.out.println("password changed");
		}
		if (adressmail != "") {
			updatingclient.setMailadress(adressmail);

		}
		if (phonenumber != null) {
			updatingclient.setPhonenumber(phonenumber);
			System.out.println("phonenumber changed");
		}
		clientBusiness.updateClient(updatingclient);
		password = null;
		adressmail = null;
		phonenumber = null;
		return "/client.xhtml?faces-redirection=true";
	}

	public String cancelReservation(Integer idreservation) {
		Reservation reservationcanceled = reservationBusiness.findreservation(idreservation);
		reservationcanceled.setAvailable(false);
		reservationBusiness.save(reservationcanceled);
		
		return "/confirmcancel.xhtml?faces-redirection=true";
	}

	public String statusReservation(Reservation reservation) {
		LocalDate now = java.time.LocalDate.now();
		String status = "";

		if (!reservation.isAvailable()) {
			status = "Annulé";
		} else {
			if (reservation.getCheckoutdate().compareTo(now) < 0) {
				status = "Séjour terminé.";
			}
			else {
				status = "En cours";
			}

		}

		return status;
	}
	
	public String getChosenRservation(Reservation chosenReservation) {
		reservation = chosenReservation;
		return "/review.xhtml?faces-redirection=true";
	}
	
	public String writeReview() {
		if (rating!=null&& rating<6&& rating >0) {
			Review newReview = new Review(null, rating, comment, client, reservation);
			reservation.setReview(newReview);
			
			reviewBusiness.writeReview(newReview);
			reservationBusiness.save(reservation);
			rating = null;
			comment = null;
			return "/confirmreview.xhtml?faces-redirection=true";
		}
		else {
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN ,
					"Veuillez donner une note entre 1 et 5", "Veuillez donner une note entre 1 et 5");
			FacesContext.getCurrentInstance().addMessage("reviewForm:inpreview", facesMessage);
			return "/review.xhtml?faces-redirection=true";
		}
	}

	public String getClientReviews() {

		reviews = reviewBusiness.findReviews(client);

		
		return "/clientreview.xhtml?faces-redirection=true";
	}
	
	public String reservationCheck(Reservation reservation) {
		String condition = null;
		LocalDate now = java.time.LocalDate.now();
		if (reservation.getCheckindate().compareTo(now) > 0 && reservation.isAvailable() ) {
			condition = "true";
		}
		else {
			condition = "false";
		}
		return condition;
	}
	
	public String WriteReviewCheck (Reservation reservation) {
		String condition = null;
		LocalDate now = java.time.LocalDate.now();
		
		if (reservation.getCheckoutdate().compareTo(now) < 0 && reservation.isAvailable()&& reservation.getReview()==null) {
			condition = "true";
		} else {
			condition = "false";
		}
		return condition;
	}
	
	public String signup() throws ParseException, NoSuchAlgorithmException {
//		System.out.println("entrer dans signup");
//		System.out.println(newsurname);
//		System.out.println(newname);
//		
//		System.out.println(newphonenumber);
//		System.out.println(newadressmail);
//		System.out.println(newlogin);
//		System.out.println(newpassword);
//		System.out.println(inputSignupCheck(newlogin));
		String forward;
		String cumin = accountBusiness.creatCumin();
		
		String newHashedpassword = accountBusiness.hashPassword(cumin,newpassword);
//		System.out.println(newHashedpassword);
		
		if (newpassword.compareTo(confirmnewpassword)==0 && newlogin!="" && inputSignupCheck(newlogin)&& newbirthdaystring!=""&& newsurname!=""&& newname !="") {
			LocalDate now = java.time.LocalDate.now();
			System.out.println("now" + now);
			DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			System.out.println("dateFormat" + dateFormat);
			LocalDate newbirthday = dateFormat.parse(newbirthdaystring).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			System.out.println("newbirthday" + newbirthday);
			
			Client newClient = new Client(null, newsurname, newname, newbirthday, newphonenumber, 
					newadressmail, now, newlogin, newHashedpassword, now, 
					false, false, null, null, cumin);
			
			clientBusiness.updateClient(newClient);
			System.out.println("c'est inséré");
			forward = "/confirmsignup.xhtml?faces-redirection=true";
		} else {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN ,
					"Veuillez remplir tous les champs nécessaires.", "Veuillez remplir tous les champs nécessaires.");
			FacesContext.getCurrentInstance().addMessage("signupForm:inpsignupfail", facesMessage);
			System.out.println("password pas bon");
			forward = "/newclientprofil.xhtml?faces-redirection=true";
		}
//		System.out.println(forward);
		newsurname = null;
		newname= null;
		newphonenumber= null;
		newadressmail= null;
		newlogin= null;
		newpassword= null;
		confirmnewpassword= null;
		newbirthdaystring= null;
		return forward;
	}

	
	public boolean isConnected() {
		return client != null;
	}
	public boolean isNotConnected() {
		return client == null;
	}
	
	

	public String disconnect() {
		
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.invalidate();
		client = null;
		return "/confirmdisconnected.xhtml?faces-redirection=true";
	}

	public String redirectionAfterLog(boolean isconnected) throws IOException {
		String forward = null;
		
		if (isconnected) {
			System.out.println("loging in");
			forward = "/clientmain.xhtml?faces-redirection=true";
			
		}
		else {
			System.out.println("need log in");
			forward = "/login.xhtml?faces-redirection=true";
		}
		System.out.println(forward);
		return forward;
	}
	
	
	
	public ClientAccountManagedBean() {
		super();
	}

	


	public ClientAccountManagedBean(Client client, String login, Client updatingclient, String password,
			String adressmail, Integer phonenumber, Set<Review> reviews, Set<Reservation> reservations, Integer rating,
			String comment, Reservation reservation, String newlogin, String newpassword, String confirmnewpassword,
			String newsurname, String newname, String newbirthdaystring, String newadressmail, Integer newphonenumber
			) {
		super();
		this.client = client;
		this.login = login;
		this.updatingclient = updatingclient;
		this.password = password;
		this.adressmail = adressmail;
		this.phonenumber = phonenumber;
		this.reviews = reviews;
		this.reservations = reservations;
		this.rating = rating;
		this.comment = comment;
		this.reservation = reservation;
		this.newlogin = newlogin;
		this.newpassword = newpassword;
		this.confirmnewpassword = confirmnewpassword;
		this.newsurname = newsurname;
		this.newname = newname;
		this.newbirthdaystring = newbirthdaystring;
		this.newadressmail = newadressmail;
		this.newphonenumber = newphonenumber;
		
	}

	public Set<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Client getUpdatingclient() {
		return updatingclient;
	}

	public void setUpdatingclient(Client updatingclient) {
		this.updatingclient = updatingclient;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAdressmail() {
		return adressmail;
	}

	public void setAdressmail(String adressmail) {
		this.adressmail = adressmail;
	}

	public Integer getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(Integer phonenumber) {
		this.phonenumber = phonenumber;
	}

	public Set<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public String getNewlogin() {
		return newlogin;
	}

	public void setNewlogin(String newlogin) {
		this.newlogin = newlogin;
	}

	public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public String getConfirmnewpassword() {
		return confirmnewpassword;
	}

	public void setConfirmnewpassword(String confirmnewpassword) {
		this.confirmnewpassword = confirmnewpassword;
	}

	public String getNewsurname() {
		return newsurname;
	}

	public void setNewsurname(String newsurname) {
		this.newsurname = newsurname;
	}

	public String getNewname() {
		return newname;
	}

	public void setNewname(String newname) {
		this.newname = newname;
	}

	public String getNewbirthdaystring() {
		return newbirthdaystring;
	}

	public void setNewbirthdaystring(String newbirthdaystring) {
		this.newbirthdaystring = newbirthdaystring;
	}

	public String getNewadressmail() {
		return newadressmail;
	}

	public void setNewadressmail(String newadressmail) {
		this.newadressmail = newadressmail;
	}

	public Integer getNewphonenumber() {
		return newphonenumber;
	}

	public void setNewphonenumber(Integer newphonenumber) {
		this.newphonenumber = newphonenumber;
	}



	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}




	

}
