package fr.hcj.cbr.CBRSpring.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Hotel;
import fr.hcj.cbr.CBRSpring.model.Reservation;
import fr.hcj.cbr.CBRSpring.service.BedroomIBusiness;
import fr.hcj.cbr.CBRSpring.service.HotelIBusiness;
import fr.hcj.cbr.CBRSpring.service.ReservationIBusiness;

@Controller(value = "mbBedroom")
@Scope(value = "session")
public class BedroomManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Hotel hotel;
	private Set<Bedroom> bedrooms;
	private Integer idhotel;
	private Integer idbedroom;
	private Client client;
	private Reservation reservation;

	@Autowired
	private BedroomIBusiness bedroombusiness;
	@Autowired
	private HotelIBusiness hotelbusiness;
	@Autowired
	private ReservationIBusiness reservationBusiness;
	@Autowired
	private HotelManagedBean mbHotel;
	
	public Float findMiniPrice(Integer idhotel) {
		Float minprice = (float) 0;
		Float price = (float) 0;
		Set<Bedroom> bedrooms = bedroombusiness.findByHotel(hotelbusiness.findByIdhotel(idhotel));
		for (Bedroom bedroom : bedrooms) {
			price = bedroom.getPrice();
				if (minprice ==0) {
					minprice = price;
					
				}
				if(minprice > price) {
					minprice = price;
				} 
		}
		
		return minprice;
		
	}

	public String FindBedroomByIdhotel(Integer idhotel) {

		System.out.println("idhotel obtenu = " + idhotel);
		bedrooms = bedroombusiness.findByHotel(hotelbusiness.findByIdhotel(idhotel));
		for (Bedroom bedroom : bedrooms) {
			System.out.println("bedroom id = " + bedroom.getIdbedroom());
		}
		String forward = "/displaybedroom.xhtml?faces-redirection=true";
		return forward;
	}
	
	public String FindAvailableBedroomByIdhotel(Integer idhotel){

		bedrooms = bedroombusiness.findAvailableHotelByIdhotel(mbHotel.getCity(), mbHotel.getNumberguest(), mbHotel.getCheckindate().toString(), mbHotel.getCheckoutdate().toString(), idhotel);
		System.out.println(mbHotel.getCity());
		System.out.println(mbHotel.getNumberguest());
		System.out.println(mbHotel.getCheckindate().toString());
		System.out.println(mbHotel.getCheckoutdate().toString());
		String forward = "/displaybedroom.xhtml?faces-redirection=true";
		return forward;
		
	}
	
	public String ReserveBedroom(Integer idbedroom) throws ParseException {
		System.out.println("reserveBedroom initié");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		String forward = "";
		
		if (session.getAttribute("connectedClient") == null) {
			forward = "/login.xhtml?faces-redirection=true";
		}
		else {
			Bedroom bedroom = bedroombusiness.findByIdbedroom(idbedroom);
			DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			client =  (Client) session.getAttribute("connectedClient");
			LocalDate checkindate;
			LocalDate checkoutdate;
			if (mbHotel.getCheckin()!=""&&mbHotel.getCheckout()!="") {
				checkindate = dateFormat.parse(mbHotel.getCheckin()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				checkoutdate = dateFormat.parse(mbHotel.getCheckout()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				reservation = new Reservation(null, checkindate, checkoutdate, mbHotel.getNumberguest(), true, client, bedroom, null);
			}
			else {
				if (mbHotel.getCheckindate()!=null&&mbHotel.getCheckoutdate()!=null) {
					checkindate = dateFormat.parse(mbHotel.getCheckindate().toString()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					checkoutdate = dateFormat.parse(mbHotel.getCheckoutdate().toString()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					reservation = new Reservation(null, checkindate, checkoutdate, mbHotel.getNumberguest(), true, client, bedroom, null);
				} else {
					
				}
			}
			
			reservationBusiness.save(reservation);
			forward = "/confirmreservation.xhtml?faces-redirection=true";
			
//			System.out.println(mbHotel.getCity());
//			System.out.println(mbHotel.getNumberguest());
//			System.out.println(mbHotel.getCheckindate());
//			System.out.println(mbHotel.getCheckoutdate());
//			if (mbHotel.getCheckin()!=null&&mbHotel.getCheckout()!=null) {
//				DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
//				LocalDate checkindate = dateFormat.parse(mbHotel.getCheckin()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//				LocalDate checkoutdate = dateFormat.parse(mbHotel.getCheckout()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//				Bedroom bedroom = bedroombusiness.findByIdbedroom(idbedroom);
//				reservation = new Reservation(null, mbHotel.getCheckindate(), mbHotel.getCheckoutdate(), mbHotel.getOutputnumberguest(), true, client, bedroom, null);
//				forward = "/confirmreservation.xhtml?faces-redirection=true";
//			}
//			else if (mbHotel.getCheckindate()!= null && mbHotel.getCheckoutdate() != null) {
//				System.out.println("enrgistre reservation initié");
//				client = (Client) session.getAttribute("connectedClient");
//				System.out.println("getbedroom");
//				Bedroom bedroom = bedroombusiness.findByIdbedroom(idbedroom);
//				
//				
//				reservation = new Reservation(null, mbHotel.getCheckindate(), mbHotel.getCheckoutdate(), mbHotel.getOutputnumberguest(), true, client, bedroom, null);
//				reservationBusiness.save(reservation);
//				forward = "/confirmreservation.xhtml?faces-redirection=true";
//					
//			}
//			else {
//				System.out.println("chosedate déclanché");
//				FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
//						"Veuillez renseigner les informations nécessaires dans la barre de recherche.", 
//						"Veuillez renseigner les informations nécessaires dans la barre de recherche.");
//				FacesContext.getCurrentInstance().addMessage("NeedCheckinCheckoutMsg", facesMessage);
//				
//				forward = "/chosedate.xhtml?faces-redirection=true";
//				
//			}
		}
		return forward;
	}

	//public 
	
	public BedroomManagedBean(Hotel hotel, Set<Bedroom> bedrooms, Integer idhotel, Integer idbedroom,
			BedroomIBusiness bedroombusiness, HotelIBusiness hotelbusiness, HotelManagedBean hotelmanagedbean) {
		super();
		this.hotel = hotel;
		this.bedrooms = bedrooms;
		this.idhotel = idhotel;
		this.idbedroom = idbedroom;
		this.bedroombusiness = bedroombusiness;
		this.hotelbusiness = hotelbusiness;

	}

	public Integer getIdhotel() {
		return idhotel;
	}

	public void setIdhotel(Integer idhotel) {
		this.idhotel = idhotel;
	}

	public BedroomManagedBean() {
		super();
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Set<Bedroom> getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(Set<Bedroom> bedrooms) {
		this.bedrooms = bedrooms;
	}

	public Integer getIdbedroom() {
		return idbedroom;
	}

	public void setIdbedroom(Integer idbedroom) {
		this.idbedroom = idbedroom;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

}
