package fr.hcj.cbr.CBRSpring.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.City;
import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Hotel;
import fr.hcj.cbr.CBRSpring.model.Photo;
import fr.hcj.cbr.CBRSpring.model.Review;
import fr.hcj.cbr.CBRSpring.service.BedroomIBusiness;
import fr.hcj.cbr.CBRSpring.service.CityIBusiness;
import fr.hcj.cbr.CBRSpring.service.HotelIBusiness;
import fr.hcj.cbr.CBRSpring.service.ReviewIBusiness;

@Controller(value = "mbHotel")
@Scope(value = "session")
public class HotelManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String city;
	private Integer numberguest;
	private String checkin;
	private String checkout;
	private Set<Photo> photos;
	
	private String outputcity;
	private Integer outputnumberguest;
	private LocalDate checkindate;
	private LocalDate checkoutdate;
	


	private Set<Hotel> hotels;
	private Set<Hotel> allhotels;

	@Autowired
	private HotelIBusiness hotelBusiness;
	@Autowired
	private CityIBusiness cityBusiness;
	@Autowired
	private ReviewIBusiness reviewBusiness;
	@Autowired
	private BedroomIBusiness bedroomBusiness;

	
	public Float findMiniPrice(Integer idhotel) {
		Float minprice = (float) 0;
		Float price = (float) 0;
		Set<Bedroom> bedrooms = bedroomBusiness.findByHotel(hotelBusiness.findByIdhotel(idhotel));
		for (Bedroom bedroom : bedrooms) {
			price = bedroom.getPrice();
				if (minprice ==0) {
					minprice = price;
					
				}
				if(minprice > price) {
					minprice = price;
				} 
		}
		
		return minprice;
		
	}
	
	public String orderByPrice(){
		ArrayList<Hotel> hotelsarray = new ArrayList<Hotel>();
		
		hotelsarray.addAll(allHotel());
		System.out.println("je suis ici");
		System.out.println(hotelsarray.size());
		System.out.println("premier syso");
		System.out.println(allHotel().size());
		System.out.println("deuxieme syso");
		for (Hotel hotel : hotelsarray) {
			
			System.out.println("jy suis" + hotelsarray.size());
			System.out.println(findMiniPrice(hotel.getIdhotel()));
		}
		Collections.sort(hotelsarray, new Comparator<Hotel>() {
			
			@Override
			public int compare(Hotel o1, Hotel o2) {
				System.out.println("je suis entrer ");
				
				return Float.valueOf(findMiniPrice(o1.getIdhotel())).compareTo(findMiniPrice(o2.getIdhotel()));
			}
			
		});
		
			for (Hotel hotel : hotelsarray) {
			
			System.out.println("jy suis en fin" + hotelsarray.size());
			System.out.println(findMiniPrice(hotel.getIdhotel()));
			
		}
			
			allhotels.clear();
			allhotels.addAll(hotelsarray);
			return "/clientmain.xhtml?faces-redirection=true";
	}
	


	public String orderByPriceReverse(){
		ArrayList<Hotel> hotelsarray = new ArrayList<Hotel>();
		
		hotelsarray.addAll(allHotel());

		Collections.sort(hotelsarray, new Comparator<Hotel>() {
			
			@Override
			public int compare(Hotel o1, Hotel o2) {
				System.out.println("je suis entrer ");
				
				return (-1)*Float.valueOf(findMiniPrice(o1.getIdhotel())).compareTo(findMiniPrice(o2.getIdhotel()));
			}
			
		});
	
			allhotels.clear();
			allhotels.addAll(hotelsarray);
			return "/clientmain.xhtml?faces-redirection=true";
	}
	
	public String orderByRating(){
		ArrayList<Hotel> hotelsarray = new ArrayList<Hotel>();
		
		hotelsarray.addAll(allHotel());

		Collections.sort(hotelsarray, new Comparator<Hotel>() {
			
			@Override
			public int compare(Hotel o1, Hotel o2) {
				
				return Float.valueOf(Float.parseFloat(calculateAverageRating(o1.getIdhotel()))).compareTo(Float.parseFloat(calculateAverageRating(o2.getIdhotel())));
			}
			
		});
	
			allhotels.clear();
			allhotels.addAll(hotelsarray);
			return "/clientmain.xhtml?faces-redirection=true";
	}
	
	public String orderByRatingReverse(){
		ArrayList<Hotel> hotelsarray = new ArrayList<Hotel>();
		
		hotelsarray.addAll(allHotel());

		Collections.sort(hotelsarray, new Comparator<Hotel>() {
			
			@Override
			public int compare(Hotel o1, Hotel o2) {
				
				return (-1)*Float.valueOf(Float.parseFloat(calculateAverageRating(o1.getIdhotel()))).compareTo(Float.parseFloat(calculateAverageRating(o2.getIdhotel())));
			}
			
		});
	
			allhotels.clear();
			allhotels.addAll(hotelsarray);
			return "/clientmain.xhtml?faces-redirection=true";
	}
	
	public boolean cityIsNull(String city) {
		return city=="";
	}
	public boolean cityIsNotNull(String city) {
		return city!="";
	}
	public boolean capacityIsNull(Integer numberguest) {
		return numberguest==null;
	}
	public boolean capacityIsNotNull(Integer numberguest) {
		return numberguest!=null;
	}
	public boolean checkinIsNull(String checkin) {
		return checkin=="";
	}
	public boolean checkinIsNotNull(String checkin) {
		return checkin!="";
	}
	public boolean checkoutIsNull(String checkout) {
		return checkout=="";
	}
	public boolean checkoutIsNotNull(String checkout) {
		return checkout!="";
	}
	public boolean checkoutAndCheckinAreNotNull(){
		return checkindate!=null&&checkoutdate!=null;
	}
	public boolean conditionReservationOK() {
		return checkindate!=null&&checkoutdate!=null&&city!=""&&numberguest!=null;
	}
	public boolean conditionReservationNotOK() {
		return checkindate==null||checkoutdate==null||city==""||numberguest==null;
	}
	
	public String researchHotel() throws ParseException {

		DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		String forward = "";
		
		
		if (city != "" && numberguest != null && checkin != "" && checkout != "" ) {

			checkindate = dateFormat.parse(checkin).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			checkoutdate = dateFormat.parse(checkout).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			
			if (checkindate.compareTo(checkoutdate)<0) {
				hotels = hotelBusiness.clientReseach(city, numberguest, checkindate.toString(), checkoutdate.toString());
				
				outputcity =city;
				outputnumberguest = numberguest;
				forward = "/researchClient.xhtml?faces-redirection=true";
				System.out.println(city);
				System.out.println(numberguest);
				System.out.println(checkindate.toString());
				System.out.println(checkoutdate.toString());
			}
			else {
				FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
						"La date de départ doit être supérieur à la date d'arrivée", 
						"La date de départ doit être supérieur à la date d'arrivée");
				FacesContext.getCurrentInstance().addMessage("ReserchForm:ReserchWarning", facesMessage);
				
			}
			
			
			
			
		}
//		if (city != "" && numberguest != null) {
//			outputcity =city;
//			outputnumberguest = numberguest;
//			hotels = hotelBusiness.getHotelsByCitynameAndCapacity(city, numberguest);
//
//			forward = "/researchClient.xhtml?faces-redirection=true";
//		}
//		if (city != "") {
//			outputcity =city;
//			hotels = hotelBusiness.findByCity(researchCityBycityname());
//			forward = "/researchClient.xhtml?faces-redirection=true";
//		}
		else {
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Veuillez renseigner les informations nécessaires dans la barre de recherche.", 
					"Veuillez renseigner les informations nécessaires dans la barre de recherche.");
			FacesContext.getCurrentInstance().addMessage("ReserchForm:ReserchWarning", facesMessage);
			//FacesContext.getCurrentInstance().addMessage("researchoptions", facesMessage);
		}
		System.out.println("résultat affiché");
		
		return forward;
		
	}

	public City researchCityBycityname() {

		City c = cityBusiness.findByCityname(city);
		return c;

	}

	public Hotel reserchHotelByIdhotel(Integer idhotel) {
		return hotelBusiness.findByIdhotel(idhotel);
	}
	
	@PostConstruct
	public Set<Hotel> allHotel() {
		
		return allhotels = hotelBusiness.allHotel();
	}
	public City showCity(Integer idcity) {
		
		return cityBusiness.findByIdcity(idcity);
	}

	public String calculateAverageRating(Integer idhotel) {
		Set<Review> allReview;
		allReview = reviewBusiness.findReviewByIdhotel(idhotel);
		Float averageRating = (float) 0;
		String hotelrating;
		if (allReview.isEmpty()) {
			averageRating = (float) 0;
			hotelrating = "A tester!";
		} else {
			for (Review review : allReview) {
				averageRating = averageRating + review.getRating();
			}
				
			averageRating = averageRating/(allReview.size());
			hotelrating = averageRating.toString();
		}
		return hotelrating;
		
	}
	//-------------------------zone de test-----------------------------------------------------------------------
		public Set<Hotel> test() {
			Set<Hotel> h = new HashSet<Hotel>();
			h = hotelBusiness.test("paris", 1, "2022-03-10", "2022-03-20");
			System.out.println(h.isEmpty());
			for (Hotel hotel : h) {
				System.out.println(hotel.getHotelname());
				System.out.println("1");
			}
			return null;
		}

		public Set<Hotel> test2() {

			Set<Hotel> h = new HashSet<Hotel>();
			h = hotelBusiness.test2();
			System.out.println(h.isEmpty());
			for (Hotel hotel : h) {
				System.out.println(hotel.getHotelname());

			}
			return null;
		}
		//------------------------------------------------------------------------------------------------------------
	
	
	public HotelManagedBean() {
		super();
	}




	public HotelManagedBean(String city, Integer numberguest, String checkin, String checkout, Set<Photo> photos,
			LocalDate checkindate, LocalDate checkoutdate, Set<Hotel> hotels) {
		super();
		this.city = city;
		this.numberguest = numberguest;
		this.checkin = checkin;
		this.checkout = checkout;
		this.photos = photos;
		this.checkindate = checkindate;
		this.checkoutdate = checkoutdate;
		this.hotels = hotels;
	}
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getNumberguest() {
		return numberguest;
	}

	public void setNumberguest(Integer numberguest) {
		this.numberguest = numberguest;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckout() {
		return checkout;
	}

	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	public Set<Hotel> getHotels() {
		return hotels;
	}

	public void setHotels(Set<Hotel> hotels) {
		this.hotels = hotels;
	}

	public Set<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(Set<Photo> photos) {
		this.photos = photos;
	}
	public LocalDate getCheckindate() {
		return checkindate;
	}
	public void setCheckindate(LocalDate checkindate) {
		this.checkindate = checkindate;
	}
	public LocalDate getCheckoutdate() {
		return checkoutdate;
	}
	public void setCheckoutdate(LocalDate checkoutdate) {
		this.checkoutdate = checkoutdate;
	}
	public String getOutputcity() {
		return outputcity;
	}
	public void setOutputcity(String outputcity) {
		this.outputcity = outputcity;
	}
	public Integer getOutputnumberguest() {
		return outputnumberguest;
	}
	public void setOutputnumberguest(Integer outputnumberguest) {
		this.outputnumberguest = outputnumberguest;
	}
	public Set<Hotel> getAllhotels() {
		return allhotels;
	}
	public void setAllhotels(Set<Hotel> allhotels) {
		this.allhotels = allhotels;
	}

}
