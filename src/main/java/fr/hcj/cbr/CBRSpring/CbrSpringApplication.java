package fr.hcj.cbr.CBRSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CbrSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CbrSpringApplication.class, args);
	}

}
