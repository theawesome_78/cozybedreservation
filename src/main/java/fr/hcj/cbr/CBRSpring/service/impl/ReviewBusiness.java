package fr.hcj.cbr.CBRSpring.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Review;
import fr.hcj.cbr.CBRSpring.repository.ReviewIDAO;
import fr.hcj.cbr.CBRSpring.service.ReviewIBusiness;
@Service
public class ReviewBusiness implements ReviewIBusiness{
	@Autowired
	ReviewIDAO reviewDao;

	@Override
	public Set<Review> findReviews(Client client) {
		
		return reviewDao.findByClient(client);
	}

	@Override
	public void writeReview(Review review) {
		
		reviewDao.save(review);
	}

	@Override
	public Set<Review> findReviewByIdhotel(Integer idhotel) {
		
		return reviewDao.getReviewByIdHotel(idhotel);
	}
	
	
}
