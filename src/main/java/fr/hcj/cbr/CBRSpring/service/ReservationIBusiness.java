package fr.hcj.cbr.CBRSpring.service;

import java.time.LocalDate;
import java.util.Set;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Reservation;

public interface ReservationIBusiness {
	public void reserver(LocalDate checkindate, LocalDate checkoutdate, 
			Integer capacity,Bedroom bedroom,
			Client client);
	
	public void save(Reservation reservation);
	public Set<Reservation> findClientReservation(Client client);
	public Reservation findreservation(Integer idreservation);
}
