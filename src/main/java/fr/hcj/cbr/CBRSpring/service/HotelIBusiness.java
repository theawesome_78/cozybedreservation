package fr.hcj.cbr.CBRSpring.service;



import java.util.Set;


import fr.hcj.cbr.CBRSpring.model.City;
import fr.hcj.cbr.CBRSpring.model.Hotel;

public interface HotelIBusiness {
	public Set<Hotel> clientReseach(String cityneame, Integer numberguest, String checkin, String checkout);
	public Set<Hotel> allHotel();
	public Set<Hotel> findByCity(City city);
	public Set<Hotel> getHotelsByCitynameAndCapacity(String cityneame, Integer numberguest);
	public Hotel findByIdhotel(Integer idhotel);
	
	public Set<Hotel> test(String cityname, Integer numberguest, String checkin, String checkout);
	public Set<Hotel> test2();
}
