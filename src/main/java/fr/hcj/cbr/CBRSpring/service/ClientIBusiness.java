package fr.hcj.cbr.CBRSpring.service;

import fr.hcj.cbr.CBRSpring.model.Client;

public interface ClientIBusiness {
	public Client findClientByIdclient(Integer idclient);
	public Client updateClient(Client client);
	public Client findClientByLogin( String login);
	
	
}
