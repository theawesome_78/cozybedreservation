package fr.hcj.cbr.CBRSpring.service;

import java.util.Set;

import org.springframework.data.repository.query.Param;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.Hotel;

public interface BedroomIBusiness {
	
	public Set<Bedroom> findByHotel(Hotel hotel);
	public Bedroom findByIdbedroom(Integer idbedroom);
	public Set<Bedroom> findAvailableHotelByIdhotel(
			String city,Integer numberguest, 
			String checkin, String checkout,
			 Integer idhotel );
	public Set<Bedroom> findMiniPriceByHotel(Integer idhotel);
}
