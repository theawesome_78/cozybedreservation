package fr.hcj.cbr.CBRSpring.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.Hotel;
import fr.hcj.cbr.CBRSpring.repository.BedroomIDAO;
import fr.hcj.cbr.CBRSpring.service.BedroomIBusiness;
@Service
public class BedroomBusiness implements BedroomIBusiness{
	
	@Autowired
	BedroomIDAO bedroomDao;

	@Override
	public Set<Bedroom> findByHotel(Hotel hotel) {
		
		return bedroomDao.findByHotel(hotel);
	}

	@Override
	public Bedroom findByIdbedroom(Integer idbedroom) {
		
		return bedroomDao.findByIdbedroom(idbedroom);
	}

	@Override
	public Set<Bedroom> findAvailableHotelByIdhotel(String city, Integer numberguest, String checkin, String checkout,
			Integer idhotel) {
		
		return bedroomDao.findAvailableBedroomByHotel(city, numberguest, checkin, checkout, idhotel);
	}

	@Override
	public Set<Bedroom> findMiniPriceByHotel(Integer idhotel) {
		
		return bedroomDao.findyMinimumPriceInHotelBedrooms(idhotel);
	}


}
