package fr.hcj.cbr.CBRSpring.service.impl;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.hcj.cbr.CBRSpring.model.Bedroom;
import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Reservation;
import fr.hcj.cbr.CBRSpring.repository.ReservationIDAO;
import fr.hcj.cbr.CBRSpring.service.ReservationIBusiness;

@Service
public class ReservationBusiness implements ReservationIBusiness{
	
	@Autowired
	ReservationIDAO reservationDao;

	@Override
	public void reserver(LocalDate checkindate, LocalDate checkoutdate, Integer capacity, Bedroom bedroom,
			Client client) {
		reservationDao.createReservation(checkindate, checkoutdate, capacity, bedroom, client);
	}

	@Override
	public void save(Reservation reservation) {

		reservationDao.save(reservation);
		
	}

	@Override
	public Set<Reservation> findClientReservation(Client client) {
		return reservationDao.findByClient(client);
		
	}

	@Override
	public Reservation findreservation(Integer idreservation) {
		
		return reservationDao.findByIdreservation(idreservation);
	}
	
	
}
;