package fr.hcj.cbr.CBRSpring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.hcj.cbr.CBRSpring.model.City;
import fr.hcj.cbr.CBRSpring.repository.CityIDAO;
import fr.hcj.cbr.CBRSpring.service.CityIBusiness;
@Service
public class CityBusiness implements CityIBusiness{
	
	@Autowired
	CityIDAO cityDao;
	
	@Override
	public City findByCityname(String cityname) {
		
		return cityDao.findByCityname(cityname);
	}

	@Override
	public City findByIdcity(Integer idcity) {
		
		return cityDao.findByIdcity(idcity);
	}

}
