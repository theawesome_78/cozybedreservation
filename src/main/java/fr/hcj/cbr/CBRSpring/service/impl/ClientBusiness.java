package fr.hcj.cbr.CBRSpring.service.impl;

import java.security.SecureRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.repository.ClientIDAO;
import fr.hcj.cbr.CBRSpring.service.ClientIBusiness;
@Service
public class ClientBusiness implements ClientIBusiness{
	@Autowired
	ClientIDAO clientDao;
	
	@Override
	public Client findClientByIdclient(Integer idclient) {
		
		return clientDao.findByIdclient(idclient);
	}

	@Override
	public Client updateClient(Client client) {

		return clientDao.save(client);
	}

	@Override
	public Client findClientByLogin(String login) {
		
		return clientDao.findByLoginIs(login);
	}

	


}
