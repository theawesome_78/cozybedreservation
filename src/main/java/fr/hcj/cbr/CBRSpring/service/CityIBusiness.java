package fr.hcj.cbr.CBRSpring.service;

import org.springframework.beans.factory.annotation.Autowired;

import fr.hcj.cbr.CBRSpring.model.City;
import fr.hcj.cbr.CBRSpring.repository.CityIDAO;

public interface CityIBusiness {
	
	
	
	public City findByCityname(String cityname);
	public City findByIdcity(Integer idcity);
}
