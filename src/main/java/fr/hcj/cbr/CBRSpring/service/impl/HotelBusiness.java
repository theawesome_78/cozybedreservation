package fr.hcj.cbr.CBRSpring.service.impl;



import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import fr.hcj.cbr.CBRSpring.model.City;
import fr.hcj.cbr.CBRSpring.model.Hotel;
import fr.hcj.cbr.CBRSpring.repository.HotelIDAO;
import fr.hcj.cbr.CBRSpring.service.HotelIBusiness;
@Service
public class HotelBusiness implements HotelIBusiness{
	
	@Autowired
	HotelIDAO hotelDao;

	@Override
	public Set<Hotel> clientReseach(String city, Integer numberguest, String checkin, String checkout) {
		
		return hotelDao.getHotelsByCityCapacityCheckinCheckout( city, numberguest, checkin, checkout);
	}

	@Override
	public Set<Hotel> allHotel() {
		
		return hotelDao.getAllHotels();
	}



	@Override
	public Set<Hotel> test2() {
		
		return hotelDao.test2();
	}

	@Override
	public Set<Hotel> test(String city, Integer numberguest, String checkin, String checkout) {
	
		return hotelDao.test(city, numberguest, checkin, checkout);
	}

	@Override
	public Set<Hotel> findByCity(City city) {
		
		return hotelDao.findByCity(city);
	}

	@Override
	public Set<Hotel> getHotelsByCitynameAndCapacity(String cityneame, Integer numberguest) {
		
		return hotelDao.getHotelsByCitynameAndCapacity(cityneame, numberguest);
	}

	@Override
	public Hotel findByIdhotel(Integer idhotel) {
		
		return hotelDao.findByIdhotel(idhotel);
	}
	
	
	
}
