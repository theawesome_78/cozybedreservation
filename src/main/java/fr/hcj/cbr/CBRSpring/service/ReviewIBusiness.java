package fr.hcj.cbr.CBRSpring.service;

import java.util.Set;

import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.model.Review;

public interface ReviewIBusiness {
	public Set<Review> findReviews(Client client);
	public void writeReview(Review review);
	public Set<Review> findReviewByIdhotel(Integer idhotel);
}
