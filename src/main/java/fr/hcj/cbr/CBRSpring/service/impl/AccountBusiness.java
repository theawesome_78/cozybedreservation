package fr.hcj.cbr.CBRSpring.service.impl;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.hcj.cbr.CBRSpring.model.Client;
import fr.hcj.cbr.CBRSpring.repository.ClientIDAO;
import fr.hcj.cbr.CBRSpring.service.AccountIBusiness;
@Service
public class AccountBusiness implements AccountIBusiness{
	
	@Autowired
	ClientIDAO clientDao;
	
	@Override
	public Client connect(String login, String password) throws NoSuchAlgorithmException  {
		
		Client clientLTryingLogin = clientDao.getClientByLogin(login);
		if (clientLTryingLogin!=null) { 
			String hashedPasswordToBeChecked = hashPassword(clientLTryingLogin.getCumin(), password);
			System.out.println(hashedPasswordToBeChecked);
			System.out.println(clientLTryingLogin.getPassword());
			if (hashedPasswordToBeChecked.compareTo(clientLTryingLogin.getPassword())==0) {
				System.out.println("mot de passe ok");
				return clientLTryingLogin;
			}
			else {
				System.out.println("mot de passe non ok");
				return null;
			}
			
		} else {
			return null;
		}
		
	}
	
	@Override
	public  String hashPassword(String cumin, String password) throws NoSuchAlgorithmException {
		
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
        StringBuilder cuminedPassword = new StringBuilder();
        cuminedPassword.append(password).append(cumin);
        byte[] encodedhash = digest.digest(cuminedPassword.toString().getBytes(StandardCharsets.UTF_8));
        
        password = new String(encodedhash, StandardCharsets.UTF_8);
        return password;
	}

	@Override
	public String creatCumin() {
		System.out.println("jengage creatCumin()");
				StringBuilder stringbuilder = new StringBuilder();
		        SecureRandom random = new SecureRandom();
		        String randomString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		        for (int i = 0; i < 3; i++) {
					int randomIndex = random.nextInt(randomString.length());
					char randomChar = randomString.charAt(randomIndex);
					stringbuilder.append(randomChar);
				}
				return stringbuilder.toString();

		    }
}
