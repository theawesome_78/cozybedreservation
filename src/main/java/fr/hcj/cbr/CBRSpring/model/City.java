package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "city")
public class City implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idcity")
	private Integer idcity;
	@Column(name = "cityname")
	private String cityname;
	@Column(name = "zipcode")
	private String zipcode;
	
	@ManyToOne
	@JoinColumn(name = "idcountry")
	private Country country;
	@OneToMany(mappedBy = "city", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	private Set<Partner> partners;
	@OneToMany(mappedBy = "city", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	private Set<Hotel> hotels;
	@Override
	public String toString() {
		return "City [idcity=" + idcity + ", cityname=" + cityname + ", zipcode=" + zipcode + ", country=" + country
				+ ", partners=" + partners + ", hotels=" + hotels + "]";
	}
	public City() {
		super();
	}
	public City(Integer idcity, String cityname, String zipcode, Country country, Set<Partner> partners,
			Set<Hotel> hotels) {
		super();
		this.idcity = idcity;
		this.cityname = cityname;
		this.zipcode = zipcode;
		this.country = country;
		this.partners = partners;
		this.hotels = hotels;
	}
	public Integer getIdcity() {
		return idcity;
	}
	public void setIdcity(Integer idcity) {
		this.idcity = idcity;
	}
	public String getCityname() {
		return cityname;
	}
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	public Set<Partner> getPartners() {
		return partners;
	}
	public void setPartners(Set<Partner> partners) {
		this.partners = partners;
	}
	public Set<Hotel> getHotels() {
		return hotels;
	}
	public void setHotels(Set<Hotel> hotels) {
		this.hotels = hotels;
	}
	
	
	
}
