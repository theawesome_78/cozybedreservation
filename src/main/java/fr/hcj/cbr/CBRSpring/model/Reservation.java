package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="reservation")
public class Reservation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idreservation")
	private Integer idreservation;
	@Column(name = "checkindate")
	private LocalDate checkindate;
	@Column(name = "checkoutdate")
	private LocalDate checkoutdate;
	@Column(name = "guestnumber")
	private Integer guestnumber;
	@Column(name = "available")
	private boolean available;
	@ManyToOne
	@JoinColumn(name = "idclient")
	private Client client;
	@ManyToOne
	@JoinColumn(name = "idbedroom")
	private Bedroom bedroom;
	@OneToOne
	@JoinColumn(name = "idreview")
	private Review review;
	public Reservation() {
		super();
	}

	

	
	public Reservation(Integer idreservation, LocalDate checkindate, LocalDate checkoutdate, int guestnumber, boolean available,
			Client client, Bedroom bedroom, Review review) {
		super();
		this.idreservation = idreservation;
		this.checkindate = checkindate;
		this.checkoutdate = checkoutdate;
		this.guestnumber = guestnumber;
		this.available = available;
		this.client = client;
		this.bedroom = bedroom;
		this.review = review;
	}






	@Override
	public String toString() {
		return "Reservation [idreservation=" + idreservation + ", checkindate=" + checkindate + ", checkoutdate="
				+ checkoutdate + ", guestnumber=" + guestnumber + ", available=" + available + ", client=" + client
				+ ", bedroom=" + bedroom + ", review=" + review + "]";
	}




	public Integer getIdreservation() {
		return idreservation;
	}
	public void setIdreservation(Integer idreservation) {
		this.idreservation = idreservation;
	}
	public LocalDate getCheckindate() {
		return checkindate;
	}
	public void setCheckindate(LocalDate checkindate) {
		this.checkindate = checkindate;
	}
	public LocalDate getCheckoutdate() {
		return checkoutdate;
	}
	public void setCheckoutdate(LocalDate checkoutdate) {
		this.checkoutdate = checkoutdate;
	}
	public int getGuestnumber() {
		return guestnumber;
	}
	public void setGuestnumber(int guestnumber) {
		this.guestnumber = guestnumber;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Bedroom getBedroom() {
		return bedroom;
	}

	public void setBedroom(Bedroom bedroom) {
		this.bedroom = bedroom;
	}

	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}




	public boolean isAvailable() {
		return available;
	}




	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	
	
	
	
	

}
