package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "bedtype")
public class Bedtype implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idbedtype")
	private Integer idbedtype;
	@Column(name = "bedtype")
	private String bedtype;
	@OneToMany(mappedBy = "bedtype", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Bed> beds;
	@Override
	public String toString() {
		return "Bedtype [idbedtype=" + idbedtype + ", bedtype=" + bedtype + ", beds=" + beds + "]";
	}
	public Bedtype() {
		super();
	}
	public Bedtype(Integer idbedtype, String bedtype, Set<Bed> beds) {
		super();
		this.idbedtype = idbedtype;
		this.bedtype = bedtype;
		this.beds = beds;
	}
	public Integer getIdbedtype() {
		return idbedtype;
	}
	public void setIdbedtype(Integer idbedtype) {
		this.idbedtype = idbedtype;
	}
	public String getBedtype() {
		return bedtype;
	}
	public void setBedtype(String bedtype) {
		this.bedtype = bedtype;
	}
	public Set<Bed> getBeds() {
		return beds;
	}
	public void setBeds(Set<Bed> beds) {
		this.beds = beds;
	}
	
	
}
