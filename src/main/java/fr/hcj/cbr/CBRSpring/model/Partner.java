package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "partner")
public class Partner implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idpartner")
	private Integer idpartner;
	@Column(name = "companyname")
	private String companyname;
	@Column(name = "streetnumber")
	private Integer streetnumber;
	@Column(name = "complement")
	private String complement;
	@Column(name = "streetname")
	private String streetname;
	@Column(name = "phonenumber")
	private Integer phonenumber;
	@Column(name = "mailaddress")
	private String mailaddress;
	@Column(name = "signupdate")
	private LocalDate signupdate;
	@Column(name = "demandsignupdate")
	private LocalDate demandsignupdate;
	@Column(name = "decisiondate")
	private LocalDate  decisiondate;
	@Column(name = "login")
	private String login;
	@Column(name = "password")
	private String password;
	@Column(name = "admincheck")
	private boolean admincheck;
	@Column(name = "partncheck")
	private boolean partncheck;
	
	@OneToMany(mappedBy = "partner", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private Set<Hotel> hotels;
	
	@ManyToOne
	@JoinColumn(name = "idcity")
	private City city;
	

	

	@Override
	public String toString() {
		return "Partner [idpartner=" + idpartner + ", companyname=" + companyname + ", streetnumber=" + streetnumber
				+ ", complement=" + complement + ", streetname=" + streetname + ", phonenumber=" + phonenumber
				+ ", mailaddress=" + mailaddress + ", signupdate=" + signupdate + ", demandsignupdate="
				+ demandsignupdate + ", decisiondate=" + decisiondate + ", login=" + login + ", password=" + password
				+ ", admincheck=" + admincheck + ", partncheck=" + partncheck + ", hotels=" + hotels + ", city=" + city
				+ "]";
	}

	public Partner() {
		super();
	}


	public Partner(Integer idpartner, String companyname, Integer streetnumber, String complement, String streetname,
			Integer phonenumber, String mailaddress, LocalDate signupdate, LocalDate demandsignupdate, LocalDate decisiondate,
			String login, String password, boolean admincheck, boolean partncheck, Set<Hotel> hotels, City city) {
		super();
		this.idpartner = idpartner;
		this.companyname = companyname;
		this.streetnumber = streetnumber;
		this.complement = complement;
		this.streetname = streetname;
		this.phonenumber = phonenumber;
		this.mailaddress = mailaddress;
		this.signupdate = signupdate;
		this.demandsignupdate = demandsignupdate;
		this.decisiondate = decisiondate;
		this.login = login;
		this.password = password;
		this.admincheck = admincheck;
		this.partncheck = partncheck;
		this.hotels = hotels;
		this.city = city;
	}

	public Integer getIdpartner() {
		return idpartner;
	}
	public void setIdpartner(Integer idpartner) {
		this.idpartner = idpartner;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public Integer getStreetnumber() {
		return streetnumber;
	}
	public void setStreetnumber(Integer streetnumber) {
		this.streetnumber = streetnumber;
	}
	public String getComplement() {
		return complement;
	}
	public void setComplement(String complement) {
		this.complement = complement;
	}
	public String getStreetname() {
		return streetname;
	}
	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}
	public Integer getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(Integer phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getMailaddress() {
		return mailaddress;
	}
	public void setMailaddress(String mailaddress) {
		this.mailaddress = mailaddress;
	}
	public LocalDate getSignupdate() {
		return signupdate;
	}
	public void setSignupdate(LocalDate signupdate) {
		this.signupdate = signupdate;
	}
	public LocalDate getDemandsignupdate() {
		return demandsignupdate;
	}
	public void setDemandsignupdate(LocalDate demandsignupdate) {
		this.demandsignupdate = demandsignupdate;
	}
	public LocalDate getDecisiondate() {
		return decisiondate;
	}
	public void setDecisiondate(LocalDate decisiondate) {
		this.decisiondate = decisiondate;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

	public Set<Hotel> getHotels() {
		return hotels;
	}

	public void setHotels(Set<Hotel> hotels) {
		this.hotels = hotels;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public boolean isAdmincheck() {
		return admincheck;
	}

	public void setAdmincheck(boolean admincheck) {
		this.admincheck = admincheck;
	}

	public boolean isPartncheck() {
		return partncheck;
	}

	public void setPartncheck(boolean partncheck) {
		this.partncheck = partncheck;
	}
	
}
