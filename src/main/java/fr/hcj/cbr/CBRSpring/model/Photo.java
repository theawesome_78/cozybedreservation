package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "photo")
public class Photo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idphoto")
	private Integer idphoto;
	@Column(name = "photoname")
	private String photoname;
	@ManyToOne
	@JoinColumn(name = "idhotel")
	private Hotel hotel;
	@ManyToMany
	@JoinTable(name = "bedroom_photo", joinColumns = @JoinColumn(name="idphoto"),inverseJoinColumns = @JoinColumn( name = "idbedroom" ))
	private Set<Bedroom> bedrooms;
	

	public Photo(Integer idphoto, String photoname, Hotel hotel, Set<Bedroom> bedrooms) {
		super();
		this.idphoto = idphoto;
		this.photoname = photoname;
		this.hotel = hotel;
		this.bedrooms = bedrooms;
	}
	public Photo() {
		super();
	}

	@Override
	public String toString() {
		return "Photo [idphoto=" + idphoto + ", photoname=" + photoname + ", hotel=" + hotel + ", bedrooms=" + bedrooms
				+ "]";
	}
	public Integer getIdphoto() {
		return idphoto;
	}
	public void setIdphoto(Integer idphoto) {
		this.idphoto = idphoto;
	}
	public String getPhotoname() {
		return photoname;
	}
	public void setPhotoname(String photoname) {
		this.photoname = photoname;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	public Set<Bedroom> getBedrooms() {
		return bedrooms;
	}
	public void setBedrooms(Set<Bedroom> bedrooms) {
		this.bedrooms = bedrooms;
	}
	
}
