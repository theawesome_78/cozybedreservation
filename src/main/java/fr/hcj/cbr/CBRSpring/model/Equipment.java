package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "equipment")
public class Equipment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idequipment")
	private Integer idequipment;
	@Column(name = "equipmentname")
	private String equipmentname;
	@ManyToMany
	@JoinTable( name = "bedroom_equipment", joinColumns = @JoinColumn(name="idequipment"),inverseJoinColumns = @JoinColumn( name = "idbedroom" ))
	private Set<Bedroom> bedrooms;
	
	public Equipment() {
		super();
	}
	
	public Equipment(Integer idequipment, String equipmentname, Set<Bedroom> bedrooms) {
		super();
		this.idequipment = idequipment;
		this.equipmentname = equipmentname;
		this.bedrooms = bedrooms;
	}

	
	@Override
	public String toString() {
		return "Equipment [idequipment=" + idequipment + ", equipmentname=" + equipmentname + ", bedrooms=" + bedrooms
				+ "]";
	}

	public Integer getIdequipment() {
		return idequipment;
	}
	public void setIdequipment(Integer idequipment) {
		this.idequipment = idequipment;
	}
	public String getEquipmentname() {
		return equipmentname;
	}
	public void setEquipmentname(String equipmentname) {
		this.equipmentname = equipmentname;
	}

	public Set<Bedroom> getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(Set<Bedroom> bedrooms) {
		this.bedrooms = bedrooms;
	}
	
}
