package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="client")
public class Client implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idclient")
	private Integer idclient;
	@Column(name="surname")
	private String surname;
	@Column(name="name")
	private String name;
	@Column(name="birthday")
	private LocalDate birthday;
	@Column(name="phonenumber")
	private Integer phonenumber;
	@Column(name="mailadress")
	private String mailadress;
	@Column(name="signupdate")
	private LocalDate signupdate;
	@Column(name="login")
	private String login;
	@Column(name="password")
	private String password;
	@Column(name="demandsignupdate")
	private LocalDate demandsignupdate;
	@Column(name = "admincheck")
	private boolean admincheck;
	@Column(name = "partncheck")
	private boolean partncheck;
	@Column(name="cumin")
	private String cumin;
	
	@OneToMany (mappedBy = "client", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Set<Reservation> reservations;
	@OneToMany (mappedBy = "client", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Review> reviews;
	


	@Override
	public String toString() {
		return "Client [idclient=" + idclient + ", surname=" + surname + ", name=" + name + ", birthday=" + birthday
				+ ", phonenumber=" + phonenumber + ", mailadress=" + mailadress + ", signupdate=" + signupdate
				+ ", login=" + login + ", password=" + password + ", demandsignupdate=" + demandsignupdate
				+ ", admincheck=" + admincheck + ", partncheck=" + partncheck + ", cumin=" + cumin + ", reservations="
				+ reservations + ", reviews=" + reviews + "]";
	}

	public Client() {
		super();
	}
	
	
	public Client(Integer idclient, String surname, String name, LocalDate birthday, Integer phonenumber,
			String mailadress, LocalDate signupdate, String login, String password, LocalDate demandsignupdate,
			boolean admincheck, boolean partncheck,  Set<Reservation> reservations, Set<Review> reviews,String cumin) {
		super();
		this.idclient = idclient;
		this.surname = surname;
		this.name = name;
		this.birthday = birthday;
		this.phonenumber = phonenumber;
		this.mailadress = mailadress;
		this.signupdate = signupdate;
		this.login = login;
		this.password = password;
		this.demandsignupdate = demandsignupdate;
		this.admincheck = admincheck;
		this.partncheck = partncheck;
		
		this.reservations = reservations;
		this.reviews = reviews;
		this.cumin = cumin;
	}

	public Integer getIdclient() {
		return idclient;
	}
	public void setIdclient(Integer idclient) {
		this.idclient = idclient;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	public Integer getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(Integer phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getMailadress() {
		return mailadress;
	}
	public void setMailadress(String mailadress) {
		this.mailadress = mailadress;
	}
	public LocalDate getSignupdate() {
		return signupdate;
	}
	public void setSignupdate(LocalDate signupdate) {
		this.signupdate = signupdate;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDate getDemandsignupdate() {
		return demandsignupdate;
	}
	public void setDemandsignupdate(LocalDate demandsignupdate) {
		this.demandsignupdate = demandsignupdate;
	}
	public Set<Reservation> getReservations() {
		return reservations;
	}
	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}
	public Set<Review> getReviews() {
		return reviews;
	}
	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	public boolean isAdmincheck() {
		return admincheck;
	}

	public void setAdmincheck(boolean admincheck) {
		this.admincheck = admincheck;
	}

	public boolean isPartncheck() {
		return partncheck;
	}

	public void setPartncheck(boolean partncheck) {
		this.partncheck = partncheck;
	}

	public String getCumin() {
		return cumin;
	}

	public void setCumin(String cumin) {
		this.cumin = cumin;
	}
	
	
}
