package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.util.Set;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "bedroom")
public class Bedroom implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idbedroom")
	private Integer idbedroom;
	@Column(name = "capacity")
	private Integer capacity;
	@Column(name = "size")
	private Integer size;
	@Column(name = "price")
	private Float price;
	@ManyToOne
	@JoinColumn(name = "idhotel")
	private Hotel hotel;
	@ManyToOne
	@JoinColumn(name = "idevent")
	private Event event;
	@ManyToMany
	@JoinTable( name = "bedroom_photo", joinColumns = @JoinColumn(name="idbedroom"),inverseJoinColumns = @JoinColumn( name = "idphoto" ))
	private Set<Photo> photos;
	@OneToMany(mappedBy = "bedroom", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Bed> beds;
	@ManyToMany
	@JoinTable( name = "bedroom_equipment", joinColumns = @JoinColumn(name="idbedroom"),inverseJoinColumns = @JoinColumn( name = "idequipment" ))
	private Set<Equipment> equipments;
	@OneToMany(mappedBy = "bedroom", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Reservation> reservations;
	@Override
	public String toString() {
		return "Bedroom [idbedroom=" + idbedroom + ", capacity=" + capacity + ", size=" + size + ", price=" + price
				+ ", hotel=" + hotel + ", event=" + event + ", photos=" + photos + ", beds=" + beds + ", equipments="
				+ equipments + ", reservations=" + reservations + "]";
	}
	public Bedroom() {
		super();
	}
	public Bedroom(Integer idbedroom, Integer capacity, Integer size, Float price, Hotel hotel, Event event,
			Set<Photo> photos, Set<Bed> beds, Set<Equipment> equipments, Set<Reservation> reservations) {
		super();
		this.idbedroom = idbedroom;
		this.capacity = capacity;
		this.size = size;
		this.price = price;
		this.hotel = hotel;
		this.event = event;
		this.photos = photos;
		this.beds = beds;
		this.equipments = equipments;
		this.reservations = reservations;
	}
	public Integer getIdbedroom() {
		return idbedroom;
	}
	public void setIdbedroom(Integer idbedroom) {
		this.idbedroom = idbedroom;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public Set<Photo> getPhotos() {
		return photos;
	}
	public void setPhotos(Set<Photo> photos) {
		this.photos = photos;
	}
	public Set<Bed> getBeds() {
		return beds;
	}
	public void setBeds(Set<Bed> beds) {
		this.beds = beds;
	}
	public Set<Equipment> getEquipments() {
		return equipments;
	}
	public void setEquipments(Set<Equipment> equipments) {
		this.equipments = equipments;
	}
	public Set<Reservation> getReservations() {
		return reservations;
	}
	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}
	
	
	

}
