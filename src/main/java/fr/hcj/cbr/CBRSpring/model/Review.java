package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name = "review")
public class Review implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idreview")
	private Integer idreview;
	@Column(name = "rating")
	private Integer rating;
	@Column(name = "comment")
	private String comment;
	@ManyToOne
	@JoinColumn(name = "idclient" )
	private Client client;
	@OneToOne
	@JoinColumn(name = "idreservation" )
	private Reservation reservation;
	/*@ManyToOne
	@JoinColumn(referencedColumnName = "idhotel")
	private Hotel hotel;
	*/
	
	
	@Override
	public String toString() {
		return "Review [idreview=" + idreview + ", rating=" + rating + ", comment=" + comment + ", client=" + client
				+ ", reservation=" + reservation + /*", hotel=" + hotel +*/ "]";
	}
	public Review() {
		super();
	}
	

	public Review(Integer idreview, Integer rating, String comment, Client client, Reservation reservation) {
		super();
		this.idreview = idreview;
		this.rating = rating;
		this.comment = comment;
		this.client = client;
		this.reservation = reservation;
	}
	public Integer getIdreview() {
		return idreview;
	}
	public void setIdreview(Integer idreview) {
		this.idreview = idreview;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Reservation getReservation() {
		return reservation;
	}
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	/*
	 * public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	*/
}
