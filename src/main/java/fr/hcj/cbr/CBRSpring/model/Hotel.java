package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "hotel")
public class Hotel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idhotel")
	private Integer idhotel;
	@Column(name = "hotelname")
	private String hotelname;
	@Column(name = "streetnumber")
	private Integer streetnumber;
	@Column(name = "streetname")
	private String streetname;
	@Column(name = "complement")
	private String complement;
	@Column(name = "phonenumber")
	private Integer phonenumber;
	@Column(name = "mailaddress")
	private String mailaddress;
	@Column(name = "resume")
	private String resume;
	@Column(name = "chambercount")
	private Integer chambercount;
	
	@ManyToOne
	@JoinColumn(name = "idpartner")
	private Partner partner;
	
	@ManyToOne
	@JoinColumn(name = "idcity")
	private City city;
	
	@OneToMany(mappedBy = "hotel", cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
	private Set<Photo> photos;
	
	@ManyToMany
	@JoinTable(name = "event_hotel", joinColumns = @JoinColumn(name="idhotel"),inverseJoinColumns = @JoinColumn( name = "idevent" ))
	private Set<Event> events;
	
	@OneToMany(mappedBy = "hotel", cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	private Set<Bedroom> bedrooms;
	
	public Hotel() {
		super();
	}


	public Hotel(Integer idhotel, String hotelname, Integer streetnumber, String streetname, String complement,
			Integer phonenumber, String mailaddress, String resume, Integer chambercount, Set<Review> reviews,
			Partner partner, City city, Set<Photo> photos, Set<Event> events, Set<Bedroom> bedrooms) {
		super();
		this.idhotel = idhotel;
		this.hotelname = hotelname;
		this.streetnumber = streetnumber;
		this.streetname = streetname;
		this.complement = complement;
		this.phonenumber = phonenumber;
		this.mailaddress = mailaddress;
		this.resume = resume;
		this.chambercount = chambercount;
		this.partner = partner;
		this.city = city;
		this.photos = photos;
		this.events = events;
		this.bedrooms = bedrooms;
	}


	@Override
	public String toString() {
		return "Hotel [idhotel=" + idhotel + ", hotelname=" + hotelname + ", streetnumber=" + streetnumber
				+ ", streetname=" + streetname + ", complement=" + complement + ", phonenumber=" + phonenumber
				+ ", mailaddress=" + mailaddress + ", resume=" + resume + ", chambercount=" + chambercount
				+ /*", reviews=" + reviews +*/ ", partner=" + partner + ", city=" + city + ", photos=" + photos
				+ ", events=" + events + ", bedrooms=" + bedrooms + "]";
	}
	public Integer getIdhotel() {
		return idhotel;
	}
	public void setIdhotel(Integer idhotel) {
		this.idhotel = idhotel;
	}
	public String getHotelname() {
		return hotelname;
	}
	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}
	public Integer getStreetnumber() {
		return streetnumber;
	}
	public void setStreetnumber(Integer streetnumber) {
		this.streetnumber = streetnumber;
	}
	public String getStreetname() {
		return streetname;
	}
	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}
	public String getComplement() {
		return complement;
	}
	public void setComplement(String complement) {
		this.complement = complement;
	}
	public Integer getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(Integer phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getMailaddress() {
		return mailaddress;
	}
	public void setMailaddress(String mailaddress) {
		this.mailaddress = mailaddress;
	}
	public String getResume() {
		return resume;
	}
	public void setResume(String resume) {
		this.resume = resume;
	}
	public Integer getChambercount() {
		return chambercount;
	}
	public void setChambercount(Integer chambercount) {
		this.chambercount = chambercount;
	}

/*
	public Set<Review> getReviews() {
		return reviews;
	}


	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}
*/

	public Partner getPartner() {
		return partner;
	}


	public void setPartner(Partner partner) {
		this.partner = partner;
	}


	public City getCity() {
		return city;
	}


	public void setCity(City city) {
		this.city = city;
	}


	public Set<Photo> getPhotos() {
		return photos;
	}


	public void setPhotos(Set<Photo> photos) {
		this.photos = photos;
	}


	public Set<Event> getEvents() {
		return events;
	}


	public void setEvents(Set<Event> events) {
		this.events = events;
	}


	public Set<Bedroom> getBedrooms() {
		return bedrooms;
	}


	public void setBedrooms(Set<Bedroom> bedrooms) {
		this.bedrooms = bedrooms;
	}
	
}
