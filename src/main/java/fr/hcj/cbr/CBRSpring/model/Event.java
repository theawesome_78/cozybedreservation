package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "event")
public class Event implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idevent")
	private Integer idevent;
	@Column(name = "startevent")
	private LocalDate startevent;
	@Column(name = "endevent")
	private LocalDate endevent;
	@Column(name = "discount")
	private Integer discount;
	@ManyToMany
	@JoinTable( name = "event_hotel", joinColumns = @JoinColumn(name="idevent"),inverseJoinColumns = @JoinColumn( name = "idhotel" ))
	private Set<Hotel> hotels;
	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Bedroom> bedrooms;
	

	@Override
	public String toString() {
		return "Event [idevent=" + idevent + ", startevent=" + startevent + ", endevent=" + endevent + ", discount="
				+ discount + ", hotels=" + hotels + ", bedrooms=" + bedrooms + "]";
	}

	public Event() {
		super();
	}
	
	public Event(Integer idevent, LocalDate startevent, LocalDate endevent, Integer discount, Set<Hotel> hotels,
			Set<Bedroom> bedrooms) {
		super();
		this.idevent = idevent;
		this.startevent = startevent;
		this.endevent = endevent;
		this.discount = discount;
		this.hotels = hotels;
		this.bedrooms = bedrooms;
	}
	public Integer getIdevent() {
		return idevent;
	}
	public void setIdevent(Integer idevent) {
		this.idevent = idevent;
	}
	public LocalDate getStartevent() {
		return startevent;
	}
	public void setStartevent(LocalDate startevent) {
		this.startevent = startevent;
	}
	public LocalDate getEndevent() {
		return endevent;
	}
	public void setEndevent(LocalDate endevent) {
		this.endevent = endevent;
	}
	public Integer getDiscount() {
		return discount;
	}
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Set<Hotel> getHotels() {
		return hotels;
	}

	public void setHotels(Set<Hotel> hotels) {
		this.hotels = hotels;
	}

	public Set<Bedroom> getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(Set<Bedroom> bedrooms) {
		this.bedrooms = bedrooms;
	}
	
}
