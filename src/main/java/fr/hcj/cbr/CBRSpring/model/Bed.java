package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "bed")
public class Bed implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idbed")
	private Integer idbed;
	@Column(name = "bednumber")
	private Integer bednumber;
	@ManyToOne
	@JoinColumn(name = "idbedroom")
	private Bedroom bedroom;
	@ManyToOne
	@JoinColumn(name = "idbedtype")
	private Bedtype bedtype;
	@Override
	public String toString() {
		return "Bed [idbed=" + idbed + ", bednumber=" + bednumber + ", bedroom=" + bedroom + ", bedtype=" + bedtype
				+ "]";
	}
	public Bed() {
		super();
	}
	public Bed(Integer idbed, Integer bednumber, Bedroom bedroom, Bedtype bedtype) {
		super();
		this.idbed = idbed;
		this.bednumber = bednumber;
		this.bedroom = bedroom;
		this.bedtype = bedtype;
	}
	public Integer getIdbed() {
		return idbed;
	}
	public void setIdbed(Integer idbed) {
		this.idbed = idbed;
	}
	public Integer getBednumber() {
		return bednumber;
	}
	public void setBednumber(Integer bednumber) {
		this.bednumber = bednumber;
	}
	public Bedroom getBedroom() {
		return bedroom;
	}
	public void setBedroom(Bedroom bedroom) {
		this.bedroom = bedroom;
	}
	public Bedtype getBedtype() {
		return bedtype;
	}
	public void setBedtype(Bedtype bedtype) {
		this.bedtype = bedtype;
	}
	
	
}
