package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "administrator")
public class Administrator implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idadministrator")
	private Integer idadministrator;
	@Column(name = "surname")
	private String surname;
	@Column(name = "name")
	private String name;
	@Column(name = "birthday")
	private LocalDate birthday;
	@Column(name = "phonenumber")
	private Integer phonenumber;
	@Column(name = "mailaddress")
	private String mailaddress;
	@Column(name = "signupdate")
	private LocalDate signupdate;
	@Column(name = "login")
	private String login;
	@Column(name = "password")
	private String password;
	@Column(name = "admincheck")
	private boolean admincheck;
	@Column(name = "partncheck")
	private boolean partncheck;
	
	public Administrator() {
		super();
	}
	
	public Administrator(Integer idadministrator, String surname, String name, LocalDate birthday, Integer phonenumber,
			String mailaddress, LocalDate signupdate, String login, String password, boolean admincheck,
			boolean partncheck) {
		super();
		this.idadministrator = idadministrator;
		this.surname = surname;
		this.name = name;
		this.birthday = birthday;
		this.phonenumber = phonenumber;
		this.mailaddress = mailaddress;
		this.signupdate = signupdate;
		this.login = login;
		this.password = password;
		this.admincheck = admincheck;
		this.partncheck = partncheck;
	}

	public Integer getIdadministrator() {
		return idadministrator;
	}
	public void setIdadministrator(Integer idadministrator) {
		this.idadministrator = idadministrator;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	public Integer getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(Integer phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getMailaddress() {
		return mailaddress;
	}
	public void setMailaddress(String mailaddress) {
		this.mailaddress = mailaddress;
	}
	public LocalDate getSignupdate() {
		return signupdate;
	}
	public void setSignupdate(LocalDate signupdate) {
		this.signupdate = signupdate;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmincheck() {
		return admincheck;
	}

	public void setAdmincheck(boolean admincheck) {
		this.admincheck = admincheck;
	}

	public boolean isPartncheck() {
		return partncheck;
	}

	public void setPartncheck(boolean partncheck) {
		this.partncheck = partncheck;
	}

	@Override
	public String toString() {
		return "Administrator [idadministrator=" + idadministrator + ", surname=" + surname + ", name=" + name
				+ ", birthday=" + birthday + ", phonenumber=" + phonenumber + ", mailaddress=" + mailaddress
				+ ", signupdate=" + signupdate + ", login=" + login + ", password=" + password + ", admincheck="
				+ admincheck + ", partncheck=" + partncheck + "]";
	}
	
	
}
