package fr.hcj.cbr.CBRSpring.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "country")
public class Country implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "idcountry")
	private Integer idcountry;
	@Column(name = "countryname")
	private String countryname;
	@Column(name = "countrycode")
	private String countrycode;
	@OneToMany(mappedBy = "country", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Set<City> cities;
	
	
	@Override
	public String toString() {
		return "Country [idcountry=" + idcountry + ", countryname=" + countryname + ", countrycode=" + countrycode
				+ ", cities=" + cities + "]";
	}

	public Country() {
		super();
	}

	public Country(Integer idcountry, String countryname, String countrycode, Set<City> cities) {
		super();
		this.idcountry = idcountry;
		this.countryname = countryname;
		this.countrycode = countrycode;
		this.cities = cities;
	}
	public Integer getIdcountry() {
		return idcountry;
	}
	public void setIdcountry(Integer idcountry) {
		this.idcountry = idcountry;
	}
	public String getCountryname() {
		return countryname;
	}
	public void setCountryname(String countryname) {
		this.countryname = countryname;
	}
	public String getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	public Set<City> getCities() {
		return cities;
	}
	public void setCities(Set<City> cities) {
		this.cities = cities;
	}
	
	
}
