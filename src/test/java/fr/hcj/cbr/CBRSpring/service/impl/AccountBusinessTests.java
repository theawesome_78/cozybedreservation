package fr.hcj.cbr.CBRSpring.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class AccountBusinessTests {

	@Test
	void CreatCuminReturnNonNull() {
		AccountBusiness accBus = new AccountBusiness();
		String cumin;

		cumin = accBus.creatCumin();
		assertNotEquals("", cumin);
		
	}
	
	@Test
	void CreatCuminReturnA3LettreString() {
		//j'ai demander/imposer un string de 3lettres Dans la méthode creatCumin() 
		AccountBusiness accBus = new AccountBusiness();
		String cumin;
		cumin = accBus.creatCumin();
		int count = 0;  
       
        for(int i = 0; i < cumin.length(); i++) {  
            if(cumin.charAt(i) != ' ')  
                count++;  
        }
		assertEquals(3, count);
		
	}
	
	@Test
	void CreatCuminReturnDiffrentString() {
		AccountBusiness accBus = new AccountBusiness();
		String cumin1;
		cumin1 = accBus.creatCumin();
		String cumin2;
		cumin2 = accBus.creatCumin();
		assertNotEquals(cumin1, cumin2);
		
	}
	
	@Test
	void CreatCuminReturnAString() {
		AccountBusiness accBus = new AccountBusiness();
		assertTrue(accBus.creatCumin().getClass().equals(String.class) );
	}
	
	
	

}
