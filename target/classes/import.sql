

--
-- Dumping data for table `country`
--

INSERT INTO `country` VALUES (1,'+33','France');
INSERT INTO `country` VALUES (2,'+30','Grèce');
INSERT INTO `country` VALUES (3,'+39','Italie');
INSERT INTO `country` VALUES (4,'+32','Belgique');
INSERT INTO `country` VALUES (5,'+49','Allemagne');

--
-- Dumping data for table `city`
--

INSERT INTO `city` VALUES (1,'Paris','75000',1);
INSERT INTO `city` VALUES (2,'Rome','00100',2);
INSERT INTO `city` VALUES (3,'Athènes','10400',3);
INSERT INTO `city` VALUES (4,'Milan','20000',3);
INSERT INTO `city` VALUES (5,'Turin','10120',3);
INSERT INTO `city` VALUES (6,'Lyon','69000',1);
INSERT INTO `city` VALUES (7,'Brest','29200',1);


--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` VALUES (1,'télé');
INSERT INTO `equipment` VALUES (2,'table basse');
INSERT INTO `equipment` VALUES (3,'machine à caffée');
INSERT INTO `equipment` VALUES (4,'chaise');
INSERT INTO `equipment` VALUES (5,'porte-manteaux');
--
-- Dumping data for table `client`
--

INSERT INTO `client` VALUES ('1', false, '1987-01-10 00:00:00','BNE', '1987-01-10 00:00:00', 'Luc', '123@gmail.com', 'Luc', false, 'K��Sd�PY�A���v�JY�޶��N��', '0600124578', '1987-01-10 00:00:00', 'Vador');



--
-- Dumping data for table `partner`
--

INSERT INTO `partner` VALUES ('1', false, 'formule2','null', '1987-01-10 00:00:00','1987-01-10 00:00:00', 'f2', 'f2@outlook.com', true, 'f2', '1234567890', '1987-01-10 00:00:00', 'avenue charle', '13', '1');


--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` VALUES (1,22,NULL,'Royal hôtel','RH@g.com',612457896,'Situé à Paris, à 400 mètres de l‘Arc de Triomphe, le Royal hôtel propose des hébergements avec un restaurant, un parking privé, une salle de sport et un bar.','Rue Rene Boulanger',12,1,1);
INSERT INTO `hotel` VALUES (2,10,NULL,'Small hostel','sh@gg.com',614589645,'Situé à Rome, à moins de 200 mètres du Campo de Fiori, le Small hostel propose un service d enregistrement et de départ rapides.','rue de Bruxelles',42,2,1);
INSERT INTO `hotel` VALUES (3,15,NULL,'BnB','bnb@c.com',645687543,'Le BnB vous accueille dans le centre de Paris, à 5 minutes de marche de la Madeleine. Il propose un centre de spa et de bien-être avec un sauna.','Avenue de St Paul',2,1,1);
INSERT INTO `hotel` VALUES (4,100,NULL,'IBIZ Paris','ibisp@gmail.com',612785896,'Situé à 10 minutes à pied de l’avenue des Champs-Elysées, IBIZ Paris est un hôtel Art déco doté d’un spa, d’une salle de sport.','rue de Courcelles',11,1,1);
INSERT INTO `hotel` VALUES (5,20,NULL,'Formule 2','f2@gmail.com',612425237,'Situé dans une demeure de caractère privée du XIXe siècle, le Formule 2 propose une connexion Wi-Fi gratuite, des chambres spacieuses au décor individuel.','Rue Jean Goujon',5,1,1);
INSERT INTO `hotel` VALUES (6,150,NULL,'Premium Hôtel','ph@c.com',600007896,'Premium Hôtel se trouve dans le quartier de Montmartre, dans une maison parisienne unique et décorée selon le style de la Belle Époque. ','Avenue de Wagram',42,1,1);

--
-- Dumping data for table `bedroom`
--

INSERT INTO `bedroom` VALUES (1,2,35,15,NULL,1);
INSERT INTO `bedroom` VALUES (2,2,60,25,NULL,1);
INSERT INTO `bedroom` VALUES (3,2,100,60,NULL,1);
INSERT INTO `bedroom` VALUES (4,1,40,20,NULL,2);
INSERT INTO `bedroom` VALUES (5,2,25,23,NULL,3);
INSERT INTO `bedroom` VALUES (6,2,30,25,NULL,4);
INSERT INTO `bedroom` VALUES (7,2,35,27,NULL,5);
INSERT INTO `bedroom` VALUES (8,2,29,20,NULL,6);

--
-- Dumping data for table `bedtype`
--

INSERT INTO `bedtype` VALUES (1,'litsimple');
INSERT INTO `bedtype` VALUES (2,'litdouble');
INSERT INTO `bedtype` VALUES (3,'litkingsize');


--
-- Dumping data for table `administrator`
--
INSERT INTO `administrator` VALUES (1,true,'2022-02-05 00:00:00','admin','admin@gmail.com','admin',false,'admin',0,'2022-02-05 00:00:00','admin');

--
-- Dumping data for table `bed`
--

INSERT INTO `bed` VALUES (1,2,1,1);
INSERT INTO `bed` VALUES (2,1,2,2);
INSERT INTO `bed` VALUES (3,1,3,3);


--
-- Dumping data for table `bedroom_equipment`
--

INSERT INTO `bedroom_equipment` VALUES (1,1);
INSERT INTO `bedroom_equipment` VALUES (1,2);
INSERT INTO `bedroom_equipment` VALUES (1,3);
INSERT INTO `bedroom_equipment` VALUES (2,1);
INSERT INTO `bedroom_equipment` VALUES (2,2);
INSERT INTO `bedroom_equipment` VALUES (3,1);
INSERT INTO `bedroom_equipment` VALUES (3,2);
INSERT INTO `bedroom_equipment` VALUES (4,1);
INSERT INTO `bedroom_equipment` VALUES (4,2);



--
-- Dumping data for table `bedroom_photo`
--



--
-- Dumping data for table `event`
--

INSERT INTO `event` VALUES (1,10,'2022-02-05 00:00:00','2022-02-25 00:00:00');


--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` VALUES (1,true, '2022-01-10 00:00:00', '2022-01-20 00:00:00', 1, 1, 1,NULL);
INSERT INTO `reservation` VALUES (2,false,'2022-04-20 00:00:00','2022-04-23 00:00:00',2,2,1,NULL);
INSERT INTO `reservation` VALUES (3,true,'2022-03-01 00:00:00','2022-03-10 00:00:00',2,1,1,NULL);
INSERT INTO `reservation` VALUES (4,true,'2022-03-20 00:00:00','2022-03-31 00:00:00',2,2,1,NULL);
INSERT INTO `reservation` VALUES (5,true, '2022-03-10 00:00:00', '2022-03-20 00:00:00', 2, 8, 1,NULL);
INSERT INTO `reservation` VALUES (6,true, '2022-02-10 00:00:00', '2022-02-20 00:00:00', 2, 5, 1,NULL);
INSERT INTO `reservation` VALUES (7,true, '2022-01-10 00:00:00', '2022-01-20 00:00:00', 2, 7, 1,NULL);
INSERT INTO `reservation` VALUES (8,true, '2022-01-10 00:00:00', '2022-01-20 00:00:00', 2, 6, 1,NULL);
INSERT INTO `reservation` VALUES (9,true, '2022-01-10 00:00:00', '2022-01-20 00:00:00', 1, 4, 1,NULL);

INSERT INTO `reservation` VALUES (10,true,'2022-01-10 00:00:00','2022-01-12 00:00:00',2,1,1,NULL);
--
-- Dumping data for table `review`
--

INSERT INTO `review` VALUES (1,'立地アットホームな感じ.',3,1,10);
INSERT INTO `review` VALUES (2,'Super hôtel. Personnel sympathique, courtois et serviable. L‘hôtel est idéalement situé à proximité des Champs Elysées. Le petit déjeuner est super.',5,1,2);
INSERT INTO `review` VALUES (3,'buenas las dos.',3,1,5);
INSERT INTO `review` VALUES (4,'قريب من الشانز والنظافة والفندق عبارة عن بوتيك والاهتمام عال لديهم وليس فرع لفندق السانت ريدجس',2,1,6);
INSERT INTO `review` VALUES (5,' I loved the location, the staff were friendly',5,1,7);
INSERT INTO `review` VALUES (6,'Personnel servant le petit-déjeuner extrêmement désagréable',1,1,8);
INSERT INTO `review` VALUES (7,'Personnel très sympathique. La chambre était spacieuse, bien équipée. La salle de bain était aussi très bien (douche + baignoire) Une décoration soignée.',5,1,9);

UPDATE `reservation` SET idreview = 1 WHERE idreservation = 10;
UPDATE `reservation` SET idreview = 2 WHERE idreservation = 2;
UPDATE `reservation` SET idreview = 3 WHERE idreservation = 5;
UPDATE `reservation` SET idreview = 4 WHERE idreservation = 6;
UPDATE `reservation` SET idreview = 5 WHERE idreservation = 7;
UPDATE `reservation` SET idreview = 6 WHERE idreservation = 8;
UPDATE `reservation` SET idreview = 7 WHERE idreservation = 9;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` VALUES (1,'idhotel1-1.jpg',1);
INSERT INTO `photo` VALUES (2,'idhotel1-2.jpg',1);
INSERT INTO `photo` VALUES (3,'idhotel1-3.jpg',1);
INSERT INTO `photo` VALUES (4,'idhotel1-4.jpg',1);
INSERT INTO `photo` VALUES (5,'idhotel2-1.jpg',2);
INSERT INTO `photo` VALUES (6,'idhotel2-2.jpg',2);
INSERT INTO `photo` VALUES (7,'idhotel2-3.jpg',2);
INSERT INTO `photo` VALUES (8,'idhotel2-4.jpg',2);
INSERT INTO `photo` VALUES (9,'idhotel3-1.jpg',3);
INSERT INTO `photo` VALUES (10,'idhotel3-2.jpg',3);
INSERT INTO `photo` VALUES (11,'idhotel3-3.jpg',3);
INSERT INTO `photo` VALUES (12,'idhotel3-4.jpg',3);
INSERT INTO `photo` VALUES (13,'idhotel4-1.jpg',4);
INSERT INTO `photo` VALUES (14,'idhotel4-2.jpg',4);
INSERT INTO `photo` VALUES (15,'idhotel4-3.jpg',4);
INSERT INTO `photo` VALUES (16,'idhotel4-4.jpg',4);
INSERT INTO `photo` VALUES (17,'idhotel5-1.jpg',5);
INSERT INTO `photo` VALUES (18,'idhotel5-2.jpg',5);
INSERT INTO `photo` VALUES (19,'idhotel5-3.jpg',5);
INSERT INTO `photo` VALUES (20,'idhotel5-4.jpg',5);
INSERT INTO `photo` VALUES (21,'idhotel6-1.jpg',6);
INSERT INTO `photo` VALUES (22,'idhotel6-2.jpg',6);
INSERT INTO `photo` VALUES (23,'idhotel6-3.jpg',6);
INSERT INTO `photo` VALUES (24,'idhotel6-4.jpg',6);

